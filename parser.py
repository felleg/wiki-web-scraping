# Written by Felix Leger and Gabrielle Perron to learn how to do Web Scraping

import urllib3
import pandas as pd
from bs4 import BeautifulSoup
from datetime import date
from tqdm import tqdm

CURRENT_YEAR = date.today().year
FARTHEST_YEAR = 1300 # Wikipedia doesn't store events by date for years before 1800

MONTHS_OF_THE_YEAR = ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"]
results = {"year": [], "month": [], "day":[], "event":[]}

for year in tqdm(range(FARTHEST_YEAR, CURRENT_YEAR + 1), desc="Years since " + str(FARTHEST_YEAR)):
    url = "https://en.wikipedia.org/wiki/" + str(year)
    # Get page content
    page = urllib3.PoolManager().request('GET', url)
    # Parse page content
    soup = BeautifulSoup(page.data, "html.parser")
    # Extract the Events h2 section (id="Events"), up to next h2 section
    soup2 = BeautifulSoup(str(soup)[str(soup).find(str(soup.find(id="Events"))):str(soup).find(str(soup.find(id="Events").find_next("h2")))], "html.parser")
    # Find months entries (ul)
    for month_tag in soup2.find_all("h3"):
        # Find the month
        month = month_tag.find(attrs={"class":"mw-headline"}).getText()

        # Skip any entry that might not be a month (e.g., world population in 2000)
        analyze = False
        for m in MONTHS_OF_THE_YEAR:
            if m in month.lower():
                analyze = True
        if analyze is False:
            continue

        # Loop on month's events
        unordered_list = month_tag.find_next("ul")
        for entry in unordered_list.find_all("li"):
            # Get the date of one event
            date_tag = entry.find("a")
            if date_tag is None:
                continue
            date = date_tag.getText()
            if date.title().split(" ")[0].lower() in MONTHS_OF_THE_YEAR:
                # Sometimes, the date cannot be extracted because the event occured in all the month
                # When that happens, set date as 0
                day = date.split(" ")[1] if len(date.split(" ")) > 1 else 0

                event = entry.getText()
                results["year"] += [year]

                # Some wikipedia entries list months like "January - June", we take this into account.
                if month.lower() in MONTHS_OF_THE_YEAR:
                    results["month"] += [month]
                    results["day"] += [day]
                else:
                    # We extracted the month and date from the event description.
                    results["month"] += [date.title().split(" ")[0]]
                    try:
                        results["day"] += [date.title().split(" ")[1]]
                    except:
                        results["day"] += ["?"]

                if "\n" in event:
                    event_to_save = event[event.find("\n"):]
                elif " – " in event:
                    event_to_save = event[event.find("– ") + 2:]
                else:
                    event_to_save = event

                results["event"] += [event_to_save.strip()]

df = pd.DataFrame(results)
FILE = "historical_events"
df.to_pickle(FILE + ".pkl")
print("Generated", FILE + ".pkl")
df.to_csv(FILE + ".csv")
print("Generated", FILE + ".csv")
